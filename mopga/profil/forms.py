from django import forms

class InscriptionForm(forms.Form):

    attrs = { "type" : "password", "class": "form-control"}
    common_attrs = {"class": "form-control"}
    username = forms.CharField(label = 'Nom d\'utilisateur ', max_length=100, widget = forms.TextInput(attrs=common_attrs))
    last_name = forms.CharField(label = 'Nom ', max_length=100, widget = forms.TextInput(attrs=common_attrs))
    first_name = forms.CharField(label = 'Prenom ', max_length=100, widget = forms.TextInput(attrs=common_attrs))
    email = forms.CharField(label = 'Email ', max_length=100, widget = forms.TextInput(attrs=common_attrs))
    password = forms.CharField(label = 'Mot de passe ', widget = forms.TextInput(attrs=attrs))

class donationForm(forms.Form):
    common_attrs = {"class": "form-control"}
    amount = forms.DecimalField(label = 'Montant en euro', widget = forms.NumberInput(attrs=common_attrs))