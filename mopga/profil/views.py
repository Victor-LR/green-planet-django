from django.contrib.auth.models import User
from .forms import InscriptionForm, donationForm
from django.shortcuts import redirect
from django.template import loader
from django.http import HttpResponse

from concept.models import Concept

from .models import Profil, Donation


def registrationView(request):
    registrationTemplate = loader.get_template('registration.html')
    registrationSuccessTemplate = loader.get_template('registration-sucess.html')
    if request.method =="POST":
        form = InscriptionForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            last_name = request.POST['last_name']
            fname = request.POST['first_name']
            email = request.POST['email']
            password = request.POST['password']
            u = User.objects.create_user(username, email, password)
            u.last_name = last_name
            u.first_name = fname
            u.save()
            profil = Profil.objects.create(user=u)
            profil.save()
            return HttpResponse(registrationSuccessTemplate.render({}, request))
    else:
        form = InscriptionForm()
    return HttpResponse(registrationTemplate.render({'form': form}, request))

def profileView(request):
    if not request.user.is_authenticated:
        return redirect('/authentification')
    profileTemplate = loader.get_template('profil.html')

    current_user = request.user
    profil = Profil.objects.filter(user=current_user)[0]

    if request.method =="POST":
        form = donationForm(request.POST)
        if form.is_valid():
            amount = request.POST['amount']
            donation = Donation.objects.create(amount=amount, giver=profil)
            donation.save()
            return redirect('/profil')
    else:
        form = donationForm()
    total = 0
    for donation in Donation.objects.filter(giver=profil):
        total += donation.amount

    context = {
        'form':form,
        'concepts': Concept.objects.filter(owner=current_user),
        'profil': profil,
        'donations':Donation.objects.filter(giver=profil),
        'total': total
    }
    return HttpResponse(profileTemplate.render(context, request))
