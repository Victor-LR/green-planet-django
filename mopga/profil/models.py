from django.contrib.auth.models import User
from django.db import models

class Profil(models.Model):
  user = models.ForeignKey(User, on_delete=models.CASCADE)

class Donation(models.Model):
  amount = models.DecimalField(decimal_places=2, max_digits=10)
  date = models.DateTimeField(auto_now_add=True, blank=True)
  giver = models.ForeignKey(Profil, on_delete=models.SET_NULL, null=True)




