# Generated by Django 2.2.17 on 2021-01-21 13:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profil', '0007_remove_donation_amount'),
    ]

    operations = [
        migrations.AddField(
            model_name='donation',
            name='amount',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
            preserve_default=False,
        ),
    ]
