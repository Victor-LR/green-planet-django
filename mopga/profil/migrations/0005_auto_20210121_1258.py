# Generated by Django 2.2.17 on 2021-01-21 12:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profil', '0004_auto_20210121_1257'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donation',
            name='amount',
            field=models.DecimalField(decimal_places=2, max_digits=65),
        ),
    ]
