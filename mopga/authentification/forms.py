from django import forms
from django.forms import TextInput


class AuthentificationForm(forms.Form):

    password_attrs = { "type" : "password", "class": "form-control"}
    username_attrs = {"class": "form-control"}
    username = forms.CharField(label = 'Nom d\'utilisateur ', max_length=100, widget = forms.TextInput(attrs=username_attrs))
    password = forms.CharField(label = 'Mot de passe ', widget = forms.TextInput(attrs=password_attrs))
