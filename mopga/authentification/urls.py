from django.conf.urls import include, url

from .views import *

urlpatterns = [
    url(r'^$', view_authentification, name='view_authentification'),
    url(r'logout$', view_deconnexion),
]