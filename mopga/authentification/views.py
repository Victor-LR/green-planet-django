from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.contrib import messages

from .forms import AuthentificationForm
from django.contrib.auth import logout


# Create your views here.
def view_authentification(request):
	if request.method == "POST":
		form = AuthentificationForm(request.POST)
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request, username=username, password=password)
		if user is not None:
			login(request, user)
			return HttpResponseRedirect('/profil/')
		else:
			messages.error(request, 'Nom d\'utilisateur ou mot de passe incorrect')
			return redirect('/authentification')
	else:
		form = AuthentificationForm()
	return render(request, "authentification.html", {'form': form})

def view_deconnexion(request):
    logout(request)
    return render(request,'deconnexion.html')
