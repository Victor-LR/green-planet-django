from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name = 'list_concepts'),
    url(r'^add-concept/$', addConceptView),
    url(r'^(?P<concept_id>[0-9]+)/$', conceptDetailView),
]