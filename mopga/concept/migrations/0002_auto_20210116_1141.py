# Generated by Django 2.2.17 on 2021-01-16 11:41

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('concept', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='concept',
            name='decription',
            field=models.CharField(blank=True, max_length=500),
        ),
        migrations.AddField(
            model_name='concept',
            name='lastModified',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='concept',
            name='mark',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='concept',
            name='owner',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
