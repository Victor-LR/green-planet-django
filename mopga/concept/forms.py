from django.forms import ModelForm, Textarea, forms, TextInput, ChoiceField, Select
from .models import Concept
from .models import Evaluation
from django import forms

class AddConceptForm(ModelForm):
    class Meta:
        model = Concept
        fields = ['title', 'description', 'location', 'cost']
        widgets = {
            'title': TextInput(attrs={'class': 'form-control'}),
            'description': Textarea(attrs={'class': 'form-control'}),
            'location': TextInput(attrs={'class': 'form-control'}),
            'cost': TextInput(attrs={'class': 'form-control'}),
        }

class AddEvaluationForm(forms.Form):
    common_attrs = {"class": "form-control"}
    choices = [('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')]
    Note = forms.ChoiceField(choices=choices, widget = forms.Select(attrs=common_attrs))
