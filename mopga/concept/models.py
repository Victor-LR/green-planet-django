
# Create your models here.
from django.db import models
from django.contrib.auth.models import User
from django.db.models import DateTimeField
from django.utils.timezone import now


class Concept(models.Model):
  title = models.CharField(max_length=200)
  description = models.CharField(max_length=500, blank=True)
  mark = models.CharField(max_length=200, blank=True)
  # quand l'utilisateur est suuprimé ses projet sont aussi supprimés
  owner = models.ForeignKey(User, on_delete=models.CASCADE)
  lastModified = models.DateTimeField(auto_now_add=True, blank=True)
  location = models.CharField(max_length=200, blank=True)
  cost = models.CharField(max_length=200, blank=True)

class Evaluation(models.Model):
  mark = models.FloatField(max_length=200)
  user = models.ForeignKey(User, on_delete=models.CASCADE)
  concept = models.ForeignKey(Concept, on_delete=models.CASCADE)







# concepts = [
#     {'title': 'Planter 1000 arbres',
#      'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
#      'budget': '',
#      'location': '',
#      'owner': 'Jerome',
#      'note': '',
#      'donations': ''},
#     {'title': 'Créer un parc naturel',
#      'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
#      'budget': '',
#      'location': '',
#      'owner': 'Omar',
#      'note': '',
#      'donations': ''},
#     {'title': 'Complexe de maisons écologiques',
#      'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
#      'budget': '',
#      'location': '',
#      'owner': 'Victor',
#      'note': '',
#      'donations': ''}
# ]
