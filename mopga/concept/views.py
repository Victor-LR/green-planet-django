from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template import loader

from .forms import AddConceptForm
from .forms import AddEvaluationForm
from .models import Concept
from .models import Evaluation
import statistics


def index(request):
    template = loader.get_template('index.html')
    context = {
        'concepts': Concept.objects.filter()
    }

    return HttpResponse(template.render(context, request))

def addConceptView(request):
    if not request.user.is_authenticated:
        return redirect('/authentification')
    print('id :')
    print(request.user.id)
    form = AddConceptForm()
    addConceptTemplate = loader.get_template('add-concept.html')
    if request.method == 'POST':
        form = AddConceptForm(request.POST)
        if form.is_valid():
            form.owner = User.objects.get(pk=request.user.id)
            obj = form.save(commit=False)
            obj.owner = User.objects.get(pk=request.user.id)
            obj.save()
            print(form)
            return redirect('/profil')
            # HttpResponseRedirect(reverse('list_concepts'))
    context = {
        'form':form
    }
    return HttpResponse(addConceptTemplate.render(context, request))

def conceptDetailView(request, concept_id):
    template = loader.get_template('concept.html')
    concept=Concept.objects.filter(id=concept_id)[0]

    if request.method == 'POST':
        form = AddEvaluationForm(request.POST)
        if form.is_valid():
            if not request.user.is_authenticated:
                return redirect('/authentification')
            mark = request.POST['Note']
            nb = Evaluation.objects.filter(user=request.user, concept=concept).count()
            print(nb)
            if(nb > 0):
                print("dans if")
                evaluation =  Evaluation.objects.filter(user=request.user, concept=concept)[0]
                Evaluation.objects.filter(pk=evaluation.id).update(mark=mark)
            else:
                evaluation = Evaluation.objects.create(mark=mark, user=request.user, concept=concept)
                evaluation.save()
    else:
        form = AddEvaluationForm()
    evaluations = Evaluation.objects.filter(concept=concept)
    score = 0
    sum = 0
    len = 0
    for e in evaluations:
        sum += e.mark
        len += 1
    if(len):
        score = sum/len
    context = {
        'score': score,
        'concept': Concept.objects.filter(id=concept_id)[0],
        'form':form,
    }
    return HttpResponse(template.render(context, request))













