#!/bin/bash
sudo docker-compose up --build -d
sleep 5
sudo docker container exec -it greenplanetdjango_db_1 apt -yq update
sudo docker container exec -it greenplanetdjango_db_1 apt install -yq git
sudo docker container exec -it greenplanetdjango_db_1 git clone https://gitlab.com/Victor-LR/green-planet-django green-planet-django
sudo docker container exec -it greenplanetdjango_db_1 cp ./green-planet-django/db_tables.sql ./db_tables.sql
sudo docker container exec -i greenplanetdjango_db_1 mysql -u root -pmysqladmin decouverte < ./db_tables.sql

#exporter bdd :  mysqldump -u root -p decouverte > db_tables.sql
