# Green Planet django

## Lancer le projet en local :

Dans le dossier _green-planet-django_ activer l'environnement :
```bash=
source venv/bin/activate
```

Installer les dépendances:
```bash=
sudo apt-get install python-virtualenv python-pip libmysqlclient-dev python3-dev
pip install -r requirements.txt
```

Créer la base de donnnées MYSQL:
```bash=
sudo apt install mysql-server
sudo systemctl start mysql
sudo mysql -uroot
```
```mysql=
CREATE USER djangoadmin@localhost IDENTIFIED by 'django';
CREATE DATABASE decouverte CHARACTER SET utf8;
GRANT ALL on decouverte.* to djangoadmin@localhost;
flush privileges;
quit;
```

Enuite pour lancer le projet aller dans _green-planet-django/mopga_ puis lancer:
```bash=
python manage.py runserver
```


## Lancer le projet via docker :

Vérifier que docker est bien installé et le service lancé :
```bash=
sudo apt-get install docker-engine
sudo service docker start
```

Pour déployer le projet dans des conteneurs lancer le scipt:
```bash=
./lancement_docker.sh
```

Pour stopper les conteneurs et les supprimer :
```bash=
sudo docker-compose down
```

Si il y a le moindre problème au premier lancement, n'hésitez pas à supprimer les conteneurs avec `sudo docker-compose down` 
et relancer directecment le script `./lancement_docker.sh`

Adresse local : 127.0.0.1:8000
